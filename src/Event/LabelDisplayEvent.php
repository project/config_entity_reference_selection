<?php

namespace Drupal\config_entity_reference_selection\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * The label display event.
 */
class LabelDisplayEvent extends Event {

  /**
   * The config entity.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $entity;

  /**
   * The label to display for the entity.
   *
   * @var string
   */
  protected $label;

  /**
   * Creates a new label display event.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The config entity.
   */
  public function __construct(ConfigEntityInterface $entity) {
    $this->entity = $entity;
    $this->label = $entity->label();
  }

  /**
   * Gets the entity.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityInterface
   *   The config entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Gets the label.
   *
   * @return string
   *   The label.
   */
  public function getLabel() {
    return $this->label ?? $this->entity->label();
  }

  /**
   * Sets the label.
   *
   * @param string $label
   *   The label to set.
   */
  public function setLabel($label) {
    $this->label = $label;
  }

}
