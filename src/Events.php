<?php

namespace Drupal\config_entity_reference_selection;

/**
 * Defines the events emitted by this module.
 */
class Events {

  /**
   * An event type emitted when config entity labels are being displayed.
   */
  public const LABEL_DISPLAY = 'config_entity_reference_selection_label_display';

}
