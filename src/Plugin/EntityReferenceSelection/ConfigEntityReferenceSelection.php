<?php

namespace Drupal\config_entity_reference_selection\Plugin\EntityReferenceSelection;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\config_entity_reference_selection\Event\LabelDisplayEvent;
use Drupal\config_entity_reference_selection\Events;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base plugin for config entity reference selection plugins.
 *
 * @EntityReferenceSelection(
 *   id = "config",
 *   label = @Translation("Config: entity reference selection"),
 *   group = "config",
 *   weight = 1,
 *   deriver = "Drupal\config_entity_reference_selection\Plugin\Derivative\ConfigEntityReferenceSelection"
 * )
 */
class ConfigEntityReferenceSelection extends DefaultSelection {

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->eventDispatcher = $container->get('event_dispatcher');

    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory */
    $logger_channel_factory = $container->get('logger.factory');

    $instance->logger = $logger_channel_factory->get('config_entity_reference_selection');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'filter' => [
        'allowed_ids' => NULL,
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * Attempts to retrieve the target type from configuration.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface|null
   *   The entity type that is being targeted, or NULL on error.
   */
  protected function getTargetType() {
    $target_type = NULL;
    $entity_type_id = $this->getConfiguration()['target_type'];
    try {
      $target_type = $this->entityTypeManager->getDefinition($entity_type_id);
    }
    /* @noinspection BadExceptionsProcessingInspection */
    catch (PluginNotFoundException $e) {
      $this->logger->critical('Entity type definition for %target_type failed to load.', [
        '%target_type' => $entity_type_id,
      ]);
    }
    return $target_type;
  }

  /**
   * Attempts to retrieve the selection options for the target entity type.
   *
   * @return array|string[]
   *   An associative array of options if any exist.
   */
  protected function getOptions() {
    $options = [];
    $entity_type_id = $this->getConfiguration()['target_type'];

    try {
      $entities = $this->entityTypeManager->getStorage($entity_type_id)->loadMultiple();
      foreach ($entities as $entity) {
        /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
        // Ensure that the user has permission to view the entity label.
        if ($entity->access('view label')) {
          $event = new LabelDisplayEvent($entity);
          $this->eventDispatcher->dispatch($event, Events::LABEL_DISPLAY);
          $options[$entity->id()] = $event->getLabel();
        }
      }
    }
    /* @noinspection BadExceptionsProcessingInspection */
    catch (InvalidPluginDefinitionException | PluginException $e) {
      $this->logger->critical('Entity storage for %target_type failed to load.', [
        '%target_type' => $entity_type_id,
      ]);
    }
    asort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);

    if ($target_type = $this->getTargetType()) {
      $allowed = $this->getConfiguration()['filter']['allowed_ids'];
      if (!empty($allowed)) {
        $query->condition($target_type->getKey('id'), $allowed, 'IN');
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    if ($target_type = $this->getTargetType()) {
      if ($options = $this->getOptions()) {
        $plural_label = $target_type->getPluralLabel();
        $form['filter']['allowed_ids'] = [
          '#type' => 'select',
          '#title' => $this->t('Allowed @label', [
            '@label' => $plural_label,
          ]),
          '#options' => $options,
          '#multiple' => TRUE,
          '#description' => $this->t('Select the @label that users can referenced through this field. If no @label are selected, all are allowed.', [
            '@label' => $plural_label,
          ]),
          '#default_value' => $this->getConfiguration()['filter']['allowed_ids'],
        ];
      }
      else {
        $form['filter']['allowed_ids'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => 'There are no configuration entities of this type that can be referenced.',
          '#attributes' => [
            'class' => [
              'messages',
              'messages--warning',
            ],
          ],
        ];
      }
    }

    // Disable auto-create.
    $form['auto_create']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    // Ensure that the allowed_ids sequence is stored without keys.
    $value_path = ['settings', 'handler_settings', 'filter', 'allowed_ids'];
    $with_keys = $form_state->getValue($value_path, []);
    $without_keys = array_values($with_keys);
    $form_state->setValue($value_path, $without_keys);
  }

}
