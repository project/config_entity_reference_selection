<?php

namespace Drupal\config_entity_reference_selection\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A plugin deriver for config entity types.
 */
class ConfigEntityReferenceSelection extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type->entityClassImplements(ConfigEntityInterface::class)) {
        $label = $this->t('Config: Filtered by specific @entity_type', ['@entity_type' => $entity_type->getPluralLabel()]);
        $this->derivatives[$entity_type_id] = $base_plugin_definition;
        $this->derivatives[$entity_type_id]['entity_types'] = [$entity_type_id];
        $this->derivatives[$entity_type_id]['base_plugin_label'] = $label;
        $this->derivatives[$entity_type_id]['label'] = $label;
      }
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
