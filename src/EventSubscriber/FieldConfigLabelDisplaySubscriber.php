<?php

namespace Drupal\config_entity_reference_selection\EventSubscriber;

use Drupal\config_entity_reference_selection\Event\LabelDisplayEvent;
use Drupal\config_entity_reference_selection\Events;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * An event subscriber that sets more descriptive labels for fields.
 *
 * @noinspection PhpUnused
 */
class FieldConfigLabelDisplaySubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Creates a new field config label display subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      Events::LABEL_DISPLAY => 'labelDisplay',
    ];
  }

  /**
   * Sets a more descriptive label for field options.
   *
   * @param \Drupal\config_entity_reference_selection\Event\LabelDisplayEvent $event
   *   The event that is being subscribed to.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   This should never happen.
   *
   * @noinspection PhpUnused
   */
  public function labelDisplay(LabelDisplayEvent $event) {
    $entity = $event->getEntity();
    if ($entity->getEntityTypeId() === 'field_config') {
      /** @var \Drupal\Core\Field\FieldConfigInterface $entity */

      $label = $event->getLabel();

      $entity_type_id = $entity->getTargetEntityTypeId();
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

      if ($entity_type->getBundleEntityType()) {
        $label = $this->t('@entity_type - @bundle - @field', [
          '@entity_type' => $entity_type->getLabel(),
          '@bundle' => $this->entityTypeBundleInfo->getBundleInfo($entity_type_id)[$entity->getTargetBundle()]['label'],
          '@field' => $label,
        ]);
      }
      else {
        $label = $this->t('@entity_type - @field', [
          '@entity_type' => $entity_type->getLabel(),
          '@field' => $label,
        ]);
      }
      $event->setLabel($label);
    }
  }

}
