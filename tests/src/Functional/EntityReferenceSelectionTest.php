<?php

namespace Drupal\Tests\config_entity_reference_selection\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Test cases for the entity reference selection plugin implementation.
 *
 * @group config_entity_reference_selection
 */
class EntityReferenceSelectionTest extends BrowserTestBase {

  /**
   * The test field configuration.
   *
   * @var \Drupal\field\FieldConfigInterface
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'config_entity_reference_selection',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $storage = FieldStorageConfig::create([
      'field_name' => 'actions_reference',
      'entity_type' => 'entity_test',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'action',
      ],
    ]);
    $storage->save();

    $this->field = FieldConfig::create([
      'field_storage' => $storage,
      'label' => 'Actions reference',
      'bundle' => 'entity_test',
      'settings' => [
        'handler' => 'config:action',
        'handler_settings' => [
          'filter' => [
            'allowed_ids' => [],
          ],
        ],
      ],
    ]);
    $this->field->save();

    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
    $form_display = EntityFormDisplay::load('entity_test.entity_test.default');
    $form_display->setComponent('actions_reference', [
      'type' => 'options_select',
      'region' => 'content',
      'weight' => 1,
    ]);
    $form_display->save();
  }

  /**
   * Test case for the entity reference selection plugin.
   */
  public function testEntityReferenceSelection() {
    $this->drupalLogin($this->drupalCreateUser([], NULL, TRUE));
    $this->drupalGet('entity_test/add');

    // By default, the the options should not be limited.
    $assert = $this->assertSession();
    $assert->optionExists('Actions reference', 'Block the selected user(s)');
    $assert->optionExists('Actions reference', 'Cancel the selected user account(s)');

    // Limit the allowed options.
    $settings = $this->field->getSettings();
    $settings['handler_settings']['filter']['allowed_ids'] = ['user_block_user_action'];
    $this->field->setSettings($settings);
    $this->field->save();

    // The options should now be limited.
    $this->drupalGet('entity_test/add');
    $assert->optionExists('Actions reference', 'Block the selected user(s)');
    $assert->optionNotExists('Actions reference', 'Cancel the selected user account(s)');

  }

}
