<?php

namespace Drupal\Tests\config_entity_reference_selection\Unit;

use Drupal\config_entity_reference_selection\Plugin\Derivative\ConfigEntityReferenceSelection;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Test cases for the plugin deriver implementation.
 *
 * @group config_entity_reference_selection
 */
class DeriverTest extends UnitTestCase {

  /**
   * The subject under test.
   *
   * @var \Drupal\config_entity_reference_selection\Plugin\Derivative\ConfigEntityReferenceSelection
   */
  protected $instance;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $content_entity_type = $this->getMockBuilder(EntityTypeInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $content_entity_type
      ->method('entityClassImplements')
      ->willReturn(FALSE);

    $config_entity_type = $this->getMockBuilder(EntityTypeInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $config_entity_type
      ->method('entityClassImplements')
      ->willReturn(TRUE);

    $config_entity_type
      ->method('getPluralLabel')
      ->willReturn('Config entity types');

    $entity_type_manager = $this->getMockBuilder(EntityTypeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $entity_type_manager
      ->method('getDefinitions')
      ->willReturn([
        'content_entity_type' => $content_entity_type,
        'config_entity_type' => $config_entity_type,
      ]);

    $container = new ContainerBuilder();
    $container->set('string_translation', $this->getStringTranslationStub());
    $container->set('entity_type.manager', $entity_type_manager);
    \Drupal::setContainer($container);
    $this->instance = ConfigEntityReferenceSelection::create($container, 'config');
  }

  /**
   * Test case for the plugin deriver service.
   */
  public function testDeriver() {
    $base_plugin_definition = [
      'id' => 'config',
      'label' => 'Config: entity reference selection',
      'group' => 'config',
      'weight' => 1,
      'deriver' => ConfigEntityReferenceSelection::class,
    ];

    $expected = [
      'config_entity_type' => [
        'id' => 'config',
        'label' => 'Config: Filtered by specific Config entity types',
        'group' => 'config',
        'weight' => 1,
        'deriver' => ConfigEntityReferenceSelection::class,
        'entity_types' => ['config_entity_type'],
        'base_plugin_label' => 'Config: Filtered by specific Config entity types',
      ],
    ];
    $actual = $this->instance->getDerivativeDefinitions($base_plugin_definition);

    // Ensure the config entity was derived and the content entity was skipped.
    static::assertEquals($expected, $actual);
  }

}
