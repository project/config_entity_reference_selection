<?php

namespace Drupal\Tests\config_entity_reference_selection\Unit;

use Drupal\config_entity_reference_selection\Event\LabelDisplayEvent;
use Drupal\config_entity_reference_selection\EventSubscriber\FieldConfigLabelDisplaySubscriber;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Test cases for the field config label display subscriber.
 *
 * @group config_entity_reference_selection
 */
class FieldConfigLabelDisplaySubscriberTest extends UnitTestCase {

  /**
   * The subject under test.
   *
   * @var \Drupal\config_entity_reference_selection\EventSubscriber\FieldConfigLabelDisplaySubscriber
   */
  protected $instance;

  /**
   * The mock entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\Stub
   */
  protected $entityTypeManager;

  /**
   * The mock entity bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface|\PHPUnit\Framework\MockObject\Stub
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->entityTypeManager = $this->createStub(EntityTypeManagerInterface::class);

    $non_bundle_entity_type = $this->createStub(EntityTypeInterface::class);

    $non_bundle_entity_type
      ->method('getBundleEntityType')
      ->willReturn(NULL);

    $non_bundle_entity_type
      ->method('getLabel')
      ->willReturn('Non bundle entity type');

    $bundle_entity_type = $this->createStub(EntityTypeInterface::class);

    $bundle_entity_type
      ->method('getLabel')
      ->willReturn('Bundle entity type');

    $bundle_entity_type
      ->method('getBundleEntityType')
      ->willReturn('test');

    $this->entityTypeManager
      ->method('getDefinition')
      ->willReturnMap([
        ['bundle_entity_type', TRUE, $bundle_entity_type],
        ['non_bundle_entity_type', TRUE, $non_bundle_entity_type],
      ]);

    $this->entityTypeBundleInfo = $this->createStub(EntityTypeBundleInfoInterface::class);

    $this->entityTypeBundleInfo
      ->method('getBundleInfo')
      ->willReturn([
        'test_bundle' => [
          'label' => 'Test bundle',
        ],
      ]);

    $this->instance = new FieldConfigLabelDisplaySubscriber($this->entityTypeManager, $this->entityTypeBundleInfo);
    $this->instance->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * Test case for an unhandled entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function testUnhandledEntityType() {
    $entity = $this->getMockBuilder(ConfigEntityInterface::class)
      ->getMock();

    $entity
      ->method('getEntityTypeId')
      ->willReturn('not_a_field');

    $entity
      ->method('label')
      ->willReturn('Label');

    $event = new LabelDisplayEvent($entity);
    $this->instance->labelDisplay($event);
    static::assertEquals('Label', $event->getLabel());
  }

  /**
   * Test case for a bundle entity target.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function testBundleTarget() {
    $entity = $this->getMockBuilder(FieldConfigInterface::class)
      ->getMock();

    $entity
      ->method('getEntityTypeId')
      ->willReturn('field_config');

    $entity
      ->method('getTargetEntityTypeId')
      ->willReturn('bundle_entity_type');

    $entity
      ->method('getTargetBundle')
      ->willReturn('test_bundle');

    $entity
      ->method('label')
      ->willReturn('Label');

    $event = new LabelDisplayEvent($entity);
    $this->instance->labelDisplay($event);

    static::assertEquals('Bundle entity type - Test bundle - Label', $event->getLabel());
  }

  /**
   * Test case for a non-bundle entity target.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function testNonBundleTarget() {
    $entity = $this->getMockBuilder(FieldConfigInterface::class)
      ->getMock();

    $entity
      ->method('getEntityTypeId')
      ->willReturn('field_config');

    $entity
      ->method('getTargetEntityTypeId')
      ->willReturn('non_bundle_entity_type');

    $entity
      ->method('label')
      ->willReturn('Label');

    $event = new LabelDisplayEvent($entity);
    $this->instance->labelDisplay($event);

    static::assertEquals('Non bundle entity type - Label', $event->getLabel());
  }

}
